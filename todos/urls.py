from django.urls import path
from todos.views import list_todos, show_list, create_list, edit_todolist,todo_list_delete, todo_item_create, todo_item_update


urlpatterns = [
    path("items/<int:id>/edit/", todo_item_update, name="todo_item_update"),
    path("items/create/", todo_item_create, name="todo_item_create"),
    path("<int:id>/delete/", todo_list_delete, name="todo_list_delete"),
    path("<int:id>/edit/",edit_todolist, name="todo_list_update"),
    path("create/", create_list, name="todo_list_create"),
    path("<int:id>/",show_list, name="todo_list_detail"),
    path("", list_todos, name="todo_list_list"),
]