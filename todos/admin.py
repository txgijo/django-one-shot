from django.contrib import admin
from todos.models import TodoItem, TodoList


@admin.register(TodoList)                                   # Register your models here.
class TodoList(admin.ModelAdmin):                           # This defines the fields of your model
    list_display = [
        "name",
        "created_on",
]


@admin.register(TodoItem)
class TodoItem(admin.ModelAdmin):
    list_display = [
        "task",
        "due_date",
        "is_completed",
    ]
 