from django.db import models

# Create your models here.
class TodoList(models.Model):
    # This defines the fields of your model
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name        ##### for humans

class TodoItem(models.Model):
    # This defines the fields of your model
    task = models.CharField(max_length=100)
    due_date = models.DateTimeField(null=True,              ##### Makes Date Time fields optional???
                                    blank=True)
    is_completed = models.BooleanField(default=False)       ##### Boolean defaults to False
    list = models.ForeignKey(                               ##### Foreign Key
        TodoList,                                           ##### Related to TodosList
        related_name="items",                               ##### Related name of "items"
        on_delete=models.CASCADE,                           ##### Should autmatically delete,
    )                                                       ##### If to-do list is deleted




  # This tells Django how to convert our model into a string
  # when we print() it, or when the admin displays it.
#   def __str__(self):
#     return self.name