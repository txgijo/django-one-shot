from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm

# Create your views here.
def list_todos(request):
    todolists = TodoList.objects.all()
    context = {
        "todos_list": todolists, 
    }
    return render(request, "todos/list.html", context)


def show_list(request, id):
    list = get_object_or_404(TodoList, id=id)
    context = {
        "list_object": list,
    }
    return render(request, "todos/todo_list_detail.html", context)


def create_list(request):
    if request.method == "POST":
        form = TodoListForm(request.POST)
        if form.is_valid():                         ### We should use the form to validate the values
            list = form.save()                      ### and save them to the DB                                 
            return redirect("todo_list_detail", id=list.id)       ### to another page and leave the function
    else:
        form  = TodoListForm()                      ### Create an instance of the Django model form class
    context = {                                 ### Put the form in the context
        "form": form,
        }
                                                    ### Render the HTML template with the form
    return render(request, "todos/create.html", context)


def edit_todolist(request, id):
    todolist = get_object_or_404(TodoList, id=id)
    if request.method == "POST":
        form = TodoListForm(request.POST, instance=todolist)  ### creating instance of a recipe FORM
        if form.is_valid():                                   ### We should use the form to validate the values
            form.save()                                       ### and save them to the DB
            return redirect("todo_list_detail", id=id)               ### If good, redirect browser 
    else:                                                     ### to another page and leave the function
        form  = TodoListForm(instance=todolist)               ### Create an instance of the Django model form class
    context = {                                           ### Put the form in the context
        "todolist_object": todolist,
        "form": form,
    }

    return render(request, "todos/edit.html", context)  ### Render the HTML template with the form


def todo_list_delete(request, id):
    todolist = TodoList.objects.get(id=id)
    if request.method == "POST":
        todolist.delete()                            ### creating instance of a recipe FORM
        return redirect("todo_list_list")               ### If good, redirect browser 

    return render(request, "todos/delete.html")  ### Render the HTML template with the form


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():                         ### We should use the form to validate the values
            todoitem = form.save()                      ### and save them to the DB                                 
            return redirect("todo_list_detail", id=todoitem.list.id)       ### to another page and leave the function
    else:
        form  = TodoItemForm()                      ### Create an instance of the Django model form class
    context = {                                 ### Put the form in the context
        "form": form,
    }
                                                    ### Render the HTML template with the form
    return render(request, "todos/items/create.html", context)


def todo_item_update(request, id):
    todoitem = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=todoitem)  ### creating instance of a recipe FORM
        if form.is_valid():                                   ### We should use the form to validate the values
            todoitem = form.save()                                       ### and save them to the DB
            return redirect("todo_list_detail", id=todoitem.list.id)               ### If good, redirect browser 
    else:                                                     ### to another page and leave the function
        form  = TodoItemForm(instance=todoitem)               ### Create an instance of the Django model form class
    context = {                                           ### Put the form in the context
        "todoitem_object": todoitem,
        "form": form,
    }

    return render(request, "todos/items/edit.html", context)  ### Render the HTML template with the form
